# VIP Checker #

List all contacts from address book and calculate score using contact's phone number


## Tools and Frameworks
* Kotlin
* Arch Components: ViewModel, LiveData, ROOM
* Dagger 2
* RxJava 2
* Picasso
* DataBinding
