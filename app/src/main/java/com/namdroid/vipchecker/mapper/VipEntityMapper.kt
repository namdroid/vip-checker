package com.namdroid.vipchecker.mapper

import com.namdroid.vipchecker.data.FengShui
import com.namdroid.vipchecker.model.Contact
import com.namdroid.vipchecker.model.Hexagram
import com.namdroid.vipchecker.model.Vip
import com.namdroid.vipchecker.utils.Utils

open class VipEntityMapper {

    fun createNew(contact: Contact): Vip {
        var hexagram:Hexagram? = null
        contact.phoneNumber?.let {hexagram = FengShui.getHexagram(contact.phoneNumber.toString())}
        val letterBgColor = Utils.getRandomColor()
        return Vip(contact.phoneBookId,contact.displayName,contact.firstName,contact.lastName,
                contact.dateOfBirth,contact.phoneNumber,contact.email,contact.address,
                contact.imageUri,letterBgColor,false,true, hexagram)
    }


    fun updateCurrent(currVip:Vip, contact: Contact): Vip {

        currVip.phoneBookId =  contact.phoneBookId
        currVip.firstName = contact.firstName
        currVip.lastName = contact.lastName
        currVip.dateOfBirth = contact.dateOfBirth
        currVip.email = contact.email
        currVip.address = contact.address
        currVip.imageUri = contact.imageUri
        currVip.phoneNumber = contact.phoneNumber

        currVip.phoneNumber?.let {
            currVip.hexagram = FengShui.getHexagram(currVip.phoneNumber.toString())
        }

        if (!currVip.displayName.equals(contact.displayName)) {
            currVip.displayName = contact.displayName
            currVip.letterColor = Utils.getRandomColor()
        }

        return currVip
    }
}