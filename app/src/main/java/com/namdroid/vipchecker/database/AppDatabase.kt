package com.namdroid.vipchecker.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters

import com.namdroid.vipchecker.dao.VipDao
import com.namdroid.vipchecker.model.Vip

@Database(entities = [(Vip::class)], version = 2)
@TypeConverters(DateTypeConverter::class,HexagramLevelTypeConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun vipDao(): VipDao

}
