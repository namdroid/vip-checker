package com.namdroid.vipchecker.database

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.migration.Migration
import com.namdroid.vipchecker.model.Vip

/**
 * Created by namdr on 17.12.2017.
 */

object DbMigration {
    val MIGRATION_1_2: Migration = object : Migration(1, 2) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("ALTER TABLE " + Vip.TABLE_NAME + " ADD COLUMN letter_color INTEGER")
        }
    }
}
