package com.namdroid.vipchecker.database

import android.arch.persistence.room.TypeConverter
import com.namdroid.vipchecker.model.HexagramLevel

class HexagramLevelTypeConverter {

    @TypeConverter
    fun toHexagramLevel(ordinal: Int): HexagramLevel {
        return HexagramLevel.values()[ordinal]
    }

    @TypeConverter
    fun fromHexagramLevel(level: HexagramLevel): Int {
        return level.ordinal
    }
}

