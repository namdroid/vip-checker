package com.namdroid.vipchecker.model

/**
 * Created by namdr on 11.02.2018.
 */
enum class HexagramLevel {VERY_GOOD,GOOD,NORMAL,BAD,VERY_BAD}