package com.namdroid.vipchecker.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.namdroid.vipchecker.model.Vip.Companion.TABLE_NAME
import org.threeten.bp.LocalDateTime

@Entity(tableName = TABLE_NAME)
data class Vip(
        @ColumnInfo(name = "phone_book_id")
        var phoneBookId: Int?,
        var displayName: String?,
        var firstName: String?,
        var lastName: String?,
        @ColumnInfo(name = "date_of_birth")
        var dateOfBirth: LocalDateTime?,
        var phoneNumber: String?,
        var email: String?,
        var address: String?,
        var imageUri: String?,
        @ColumnInfo(name = "letter_color")
        var letterColor:Int?,
        @ColumnInfo(name = "is_favorite")
        var isFavorite: Boolean?,
        @ColumnInfo(name = "is_vip")
        var isVip: Boolean?,
        @Embedded
        var hexagram: Hexagram? = Hexagram(0,HexagramLevel.NORMAL,"","")

) {
        @PrimaryKey(autoGenerate = true)
        var id: Long? = null

        companion object {
                const val TABLE_NAME = "vips"
        }
}
