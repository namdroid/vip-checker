package com.namdroid.vipchecker.model

import android.arch.persistence.room.ColumnInfo
import android.os.Parcel
import android.os.Parcelable

import org.threeten.bp.LocalDateTime
import org.threeten.bp.temporal.ChronoUnit

class Contact : Parcelable {

    var phoneBookId: Int? = null
    var displayName: String? = null
    var firstName: String? = null
    var lastName: String? = null
    @ColumnInfo(name = DATE_FIELD)
    var dateOfBirth: LocalDateTime? = null
    var phoneNumber: String? = null
    var email: String? = null
    var address: String? = null
    var imageUri: String? = null

    val daysUntil: Long
        get() = ChronoUnit.DAYS.between(LocalDateTime.now(), dateOfBirth)

    constructor() {}

    constructor(firstName: String, lastName: String, phoneNumber: String,
                dateOfBirth: LocalDateTime, email: String, address: String) {
        this.firstName = firstName
        this.lastName = lastName
        this.dateOfBirth = dateOfBirth
        this.phoneNumber = phoneNumber
        this.email = email
        this.address = address
    }

    override fun toString(): String {
        return ("Contact{"
                + ", phoneBookId=" + phoneBookId
                + ", firstName='"
                + firstName
                + '\''.toString()
                + ", lastName='"
                + lastName
                + '\''.toString()
                + ", dateOfBirth="
                + dateOfBirth
                + ", phoneNumber='"
                + phoneNumber
                + '\''.toString()
                + ", email='"
                + email
                + '\''.toString()
                + '}'.toString())
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(this.firstName)
        dest.writeString(this.lastName)
        dest.writeSerializable(this.dateOfBirth)
        dest.writeString(this.phoneNumber)
        dest.writeString(this.email)
        dest.writeString(this.address)
    }

    protected constructor(`in`: Parcel) {
        this.firstName = `in`.readString()
        this.lastName = `in`.readString()
        this.dateOfBirth = `in`.readSerializable() as LocalDateTime
        this.phoneNumber = `in`.readString()
        this.email = `in`.readString()
        this.address = `in`.readString()
    }

    companion object {
        const val DATE_FIELD = "dateOfBirth"

        val CREATOR: Parcelable.Creator<Contact> = object : Parcelable.Creator<Contact> {
            override fun createFromParcel(source: Parcel): Contact {
                return Contact(source)
            }

            override fun newArray(size: Int): Array<Contact?> {
                return arrayOfNulls(size)
            }
        }
    }
}
