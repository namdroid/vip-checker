package com.namdroid.vipchecker.model


/**
 * Created by namnguyen on 27.07.17.
 */
data class Hexagram (val hexagramId: Int,val level:HexagramLevel,val title: String,val description: String)