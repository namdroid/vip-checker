package com.namdroid.vipchecker.repository


import android.arch.lifecycle.LiveData
import com.namdroid.vipchecker.dao.VipDao
import com.namdroid.vipchecker.data.PhoneBookHelper
import com.namdroid.vipchecker.mapper.VipEntityMapper
import com.namdroid.vipchecker.model.Vip
import io.reactivex.Completable
import timber.log.Timber
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class VipRepositoryImpl @Inject constructor(private var vipDao: VipDao) : VipRepository<Vip> {
    override fun count(): Int {
        return vipDao.getCount()
    }

    override fun get(id: Long): LiveData<Vip> {
        return vipDao.get(id)
    }


    override fun sync(): Completable {
        return Completable.fromAction {
            val phoneContacts = PhoneBookHelper.contacts
            val listToInsert = LinkedList<Vip>()
            for (phonecontact in phoneContacts) {

                val vipNew = VipEntityMapper().createNew(phonecontact)

                val vipOld: Vip? = vipDao.getByPhoneBookId(vipNew.phoneBookId!!)
                if (vipOld != null)  {
                    vipNew.id = vipOld.id
                    vipNew.letterColor = vipOld.letterColor
                    vipNew.isFavorite = vipOld.isFavorite
                    vipNew.isVip = vipOld.isVip

                    if (vipOld != vipNew) {
                        // update
                        val vipToUpdate = VipEntityMapper().updateCurrent(vipOld,phonecontact)
                        val result = vipDao.update(vipToUpdate)
                        if (result < 0) {
                            Timber.e("update error " + phonecontact.displayName)
                        }
                    }
                } else {
                    listToInsert.add(vipNew)
                }
            }

            if (!listToInsert.isEmpty()) {
                val result = vipDao.insertVips(listToInsert)
                Timber.i("insert result " + result)
            }
        }
    }



    override fun add(vip: Vip): Completable {
        return Completable.fromAction { vipDao.insert(vip) }
    }

    override fun getAll(): LiveData<List<Vip>> {
        return vipDao.all
    }

    override fun delete(vip: Vip): Completable {
        return Completable.fromAction { vipDao.delete(vip) }
    }

    override fun update(vip: Vip): Completable {
        return Completable.fromAction { vipDao.update(vip) }
    }
}
