package com.namdroid.vipchecker.repository

import android.arch.lifecycle.LiveData
import io.reactivex.Completable

interface VipRepository<T> {

    fun getAll(): LiveData<List<T>>

    fun get(id: Long): LiveData<T>

    fun add(t: T): Completable

    fun sync(): Completable

    fun delete(t: T): Completable

    fun update(t: T): Completable

    fun count(): Int
}
