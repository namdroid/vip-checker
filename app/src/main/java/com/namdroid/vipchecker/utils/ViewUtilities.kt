package com.namdroid.vipchecker.utils

import android.content.Context
import android.text.TextUtils
import android.widget.EditText
import android.widget.LinearLayout

import org.threeten.bp.LocalDateTime

object ViewUtilities {

    fun getInputFromEditTexts(mParentLayout: LinearLayout): String {
        val inputArray = arrayOfNulls<String>(mParentLayout.childCount)
        val sb = StringBuilder()
        val length = inputArray.size
        for (i in 0 until length) {
            val editText = mParentLayout.getChildAt(i) as EditText
            sb.append(editText.text.toString())
            if (i != length - 1) sb.append("-")
        }
        return sb.toString()
    }

    fun getPixelsFromDp(context: Context, dp: Int): Int {
        val density = context.resources.displayMetrics.density
        return Math.round(dp * density)
    }

    fun checkIfValueSet(text: EditText, description: String): Boolean {
        if (TextUtils.isEmpty(text.text)) {
            text.error = "Missing field " + description
            return false
        } else {
            text.error = null
            return true
        }
    }

    fun getLocalDateTimeFromString(date: String): LocalDateTime {
        val yearMonthDay = date.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val year = Integer.parseInt(yearMonthDay[0])
        val month = Integer.parseInt(yearMonthDay[1])
        val day = Integer.parseInt(yearMonthDay[2])
        return LocalDateTime.of(year, month, day, 0, 0)
    }
}
