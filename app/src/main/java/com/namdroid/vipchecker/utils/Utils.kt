package com.namdroid.vipchecker.utils

import android.content.Context
import android.graphics.Color
import android.net.ConnectivityManager
import android.os.Build
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.time.temporal.Temporal
import java.util.*

/**
 * Created by namnguyen on 29.07.17.
 */
object Utils {
    fun isNetworkConnected(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val info = connectivityManager.activeNetworkInfo
        return info != null && info.isConnected
    }

    fun daysUntil(dateOfBirth:Temporal):Long
    {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            ChronoUnit.DAYS.between(LocalDateTime.now(),dateOfBirth )
        } else {
            TODO("VERSION.SDK_INT < O")
        }
    }

    fun getRandomColor():Int {
        val randomBackgroundColor = Random()
        return Color.argb(255, randomBackgroundColor.nextInt(256),
                randomBackgroundColor.nextInt(256), randomBackgroundColor.nextInt(256))
    }
}
