package com.namdroid.vipchecker.ui.main

import android.Manifest
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v4.view.animation.FastOutLinearInInterpolator
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.transition.Explode
import android.transition.Transition
import android.util.TypedValue
import android.view.*
import com.google.firebase.auth.FirebaseAuth
import com.namdroid.vipchecker.R
import com.namdroid.vipchecker.ui.list.ContactListFragment
import com.namdroid.vipchecker.ui.list.FavListFragment
import com.namdroid.vipchecker.ui.list.VipListFragment
import com.tbruyelle.rxpermissions2.RxPermissions
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import timber.log.Timber
import javax.inject.Inject

class MainActivity : AppCompatActivity(), HasSupportFragmentInjector {

    /**
     * The [android.support.v4.view.PagerAdapter] that will provide
     * fragments for each of the sections. We use a
     * [FragmentPagerAdapter] derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * [android.support.v4.app.FragmentStatePagerAdapter].
     */
    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null

    /**
     * The [ViewPager] that will host the section contents.
     */
    private lateinit var viewPager: ViewPager

    private var mAuth: FirebaseAuth? = null

    @Inject
    lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)

        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)

        // Set up the ViewPager with the sections adapter.
        viewPager = findViewById<View>(R.id.view_pager) as ViewPager
        viewPager.adapter = mSectionsPagerAdapter

        //setting Tab layout (number of Tabs = number of ViewPager pages)
        val tabLayout: TabLayout = findViewById(R.id.tab_layout)
        tabLayout.setupWithViewPager(viewPager)

        tabLayout.getTabAt(0)?.setIcon(R.drawable.ic_vip_white)
        tabLayout.getTabAt(1)?.setIcon(R.drawable.ic_star_white_24dp)
        tabLayout.getTabAt(2)?.setIcon(R.drawable.ic_person_white_24dp)
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                //viewPager.currentItem = tab.position
                //tab.icon?.applyColorFilter(fetchPrimaryColor(application))
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                //tab.icon?.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN)
            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

        RxPermissions(this)
                .request(Manifest.permission.READ_CONTACTS)
                .subscribe { granted  ->
                    if (granted) {
                        viewModel.syncContacts()
                    }
                }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onResume() {
        super.onResume()
     /*    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
             val transition = Explode()
             transition.setDuration(600);
             transition.setInterpolator(FastOutLinearInInterpolator());
             getWindow().setReenterTransition(transition);
        } */
    }

    private fun fetchPrimaryColor(context: Context): Int {
        val typedValue = TypedValue()

        val a = context.obtainStyledAttributes(typedValue.data, intArrayOf(R.attr.colorPrimary))
        val color = a.getColor(0, 0)

        a.recycle()

        return color
    }

    fun Drawable.applyColorFilter(color: Int) {
        mutate().setColorFilter(color, PorterDuff.Mode.SRC_IN)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        return super.onOptionsItemSelected(item)

    }

    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        private var childFragments = emptyArray<Fragment>()

        init {
            childFragments = arrayOf(VipListFragment.newInstance(),
                    FavListFragment.newInstance(),
                    ContactListFragment.newInstance())
        }
        override fun getItem(position: Int): Fragment {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return childFragments[position]

        }

        override fun getCount(): Int {
            return childFragments.size
        }
    }


    private fun signInToFirebase() {

        mAuth = FirebaseAuth.getInstance()

        mAuth!!.signInAnonymously().addOnCompleteListener(this) { task ->
            if (task.isSuccessful) {
                // Sign in success, update UI with the signed-in user's information
                Timber.d("signInAnonymously:success")
                val user = mAuth!!.currentUser
            } else {
                // If sign in fails, display a message to the user.
                Timber.w("signInAnonymously:failure" + task.exception)
            }
        }
    }


    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentDispatchingAndroidInjector
    }

}
