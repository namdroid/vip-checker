package com.namdroid.vipchecker.ui.edit

import android.app.DatePickerDialog
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.InputType
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView

import com.namdroid.vipchecker.R
import com.namdroid.vipchecker.VipCheckerApp
import com.namdroid.vipchecker.injection.Injectable
import com.namdroid.vipchecker.injection.ViewModelFactory
import com.namdroid.vipchecker.model.Contact
import com.namdroid.vipchecker.utils.ViewUtilities
import com.namdroid.vipchecker.ui.vipdetail.VipViewModel

import org.threeten.bp.LocalDateTime
import javax.inject.Inject

class EditContactFragment : Fragment(), DatePickerDialog.OnDateSetListener , Injectable{

    var actionBar: ActionBar? = null
    private var firstName: EditText? = null
    private var lastName: EditText? = null
    private var dateOfBirth: EditText? = null
    private var phoneNumber: EditText? = null
    private var email: EditText? = null
    private var address: EditText? = null
    private var layoutPhone: LinearLayout? = null
    private var layoutEmail: LinearLayout? = null
    private var layoutAddress: LinearLayout? = null
    private var buttonAddPhone: TextView? = null
    private var buttonAddEmail: TextView? = null
    private var buttonAddAddress: TextView? = null

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    lateinit var addContactViewModel: VipViewModel

    internal var contact: Contact? = null
    internal var phoneIdNmbr = 0
    internal var emailIdNmbr = 0
    internal var addressIdNmbr = 0
    internal var nbrOfPhones = 1
    internal var nbrOfEmails = 1
    internal var nbrOfAddress = 1
    internal var maxLength = 3

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_add_contact, container, false)
        setupViews(view)
        setupClickListeners()
        setupViewModel()
        setHasOptionsMenu(true)
        return view
    }


    private fun setupViews(view: View) {
        firstName = view.findViewById(R.id.edit_text_first_name)
        lastName = view.findViewById(R.id.edit_text_last_name)
        dateOfBirth = view.findViewById(R.id.text_view_date_of_birth)
        layoutPhone = view.findViewById(R.id.layout_phone)
        phoneNumber = view.findViewById(R.id.edit_text_phone_number0)
        buttonAddPhone = view.findViewById(R.id.btn_addPhone)
        layoutEmail = view.findViewById(R.id.layout_email)
        email = view.findViewById(R.id.edit_text_email)
        buttonAddEmail = view.findViewById(R.id.btn_addEmail)
        layoutAddress = view.findViewById(R.id.layout_address)
        address = view.findViewById(R.id.edit_text_address)
        buttonAddAddress = view.findViewById(R.id.btn_addAddress)
        actionBar = (activity as AppCompatActivity).supportActionBar
    }

    private fun setupViewModel() {
        val vipCheckerApp = activity?.application as VipCheckerApp
        addContactViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(VipViewModel::class.java)
        firstName!!.setText(addContactViewModel.contactFirstName)
        lastName!!.setText(addContactViewModel.contactLastName)
        val dob = if (addContactViewModel.dobDateTime != null)
            addContactViewModel.dobDateTime
                    .toString()
                    .substring(0, 10)
        else
            ""
        dateOfBirth!!.setText(dob)
        phoneNumber!!.setText(addContactViewModel.contactPhone)
        email!!.setText(addContactViewModel.contactEmail)
        address!!.setText(addContactViewModel.contactAddress)
    }

    private fun setupClickListeners() {
        firstName!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                addContactViewModel.contactFirstName = s.toString()
            }
        })
        lastName!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                addContactViewModel.contactLastName = s.toString()
            }
        })
        dateOfBirth!!.setOnClickListener { _ ->
            val localDateTime = if (addContactViewModel.dobDateTime != null)
                addContactViewModel.dobDateTime
            else
                LocalDateTime.now()
            val datePickerDialog = DatePickerDialog(context, this, localDateTime!!.year,
                    localDateTime.monthValue - 1, localDateTime.dayOfMonth)
            datePickerDialog.show()
        }
        buttonAddPhone!!.setOnClickListener { _ -> createPhoneEditTextDynamically("") }
        buttonAddEmail!!.setOnClickListener { _ -> createEmailEditTextDynamically("") }
        buttonAddAddress!!.setOnClickListener { _ -> createAddressEditTextDynamically("") }
    }

    private fun allFieldsOk(): Boolean {
        if (!ViewUtilities.checkIfValueSet(firstName!!, "first name")) {
            return false
        }
        if (!ViewUtilities.checkIfValueSet(lastName!!, "last name")) {
            return false
        }
        if (!ViewUtilities.checkIfValueSet(dateOfBirth!!, "last name")) {
            return false
        }
        if (!ViewUtilities.checkIfValueSet(phoneNumber!!, "phone number")) {
            return false
        }
        return if (!ViewUtilities.checkIfValueSet(email!!, "email")) {
            false
        } else true
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        contact = (activity as ContactActivity).contact
        var toolbarTitle = resources.getString(R.string.add_contact)
        if (null != contact) {
            setContactInfo()
            toolbarTitle = resources.getString(R.string.update_contact)
        }
        actionBar!!.title = toolbarTitle
    }

    private fun setContactInfo() {
        firstName!!.setText(contact!!.firstName)
        lastName!!.setText(contact!!.lastName)
        val dob = if (contact!!.dateOfBirth != null)
            contact!!.dateOfBirth!!.toString().substring(0, 10)
        else
            ""
        dateOfBirth!!.setText(dob)
        val phoneNumbers = contact!!.phoneNumber!!.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        setPhoneInfoToUpdate(phoneNumbers)
        val emails = contact!!.email!!.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        setEmailInfoToUpdate(emails)
        if (!TextUtils.isEmpty(contact!!.address)) {
            val addresses = contact!!.address!!.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            setAddressInfoToUpdate(addresses)
        } else {
            address!!.setText("")
        }
    }

    private fun createPhoneEditTextDynamically(text: String) {
        if (nbrOfPhones < maxLength) {
            val editTxt = EditText(context)
            phoneIdNmbr++
            editTxt.hint = resources.getString(R.string.phone_number) + " " + phoneIdNmbr
            editTxt.inputType = InputType.TYPE_CLASS_PHONE
            editTxt.tag = "edit_text_phone_number" + phoneIdNmbr
            if (!TextUtils.isEmpty(text)) {
                editTxt.setText(text)
            }
            val layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT)
            val pixelsFromDp8 = context?.let { ViewUtilities.getPixelsFromDp(it, 8) }
            val pixelsFromDp24 = context?.let { ViewUtilities.getPixelsFromDp(it, 24) }
            pixelsFromDp8?.let { pixelsFromDp24?.let { it1 -> layoutParams.setMargins(it, 0, it1, pixelsFromDp8) } }
            editTxt.layoutParams = layoutParams
            layoutPhone!!.addView(editTxt)
            nbrOfPhones++
        }
    }

    private fun createEmailEditTextDynamically(text: String) {
        if (nbrOfEmails < maxLength) {
            val editTxt = EditText(context)
            emailIdNmbr++
            editTxt.hint = resources.getString(R.string.email) + " " + emailIdNmbr
            editTxt.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
            editTxt.tag = "edit_text_email" + emailIdNmbr
            if (!TextUtils.isEmpty(text)) {
                editTxt.setText(text)
            }
            val layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT)
            val pixelsFromDp8 = context?.let { ViewUtilities.getPixelsFromDp(it, 8) }
            val pixelsFromDp24 = context?.let { ViewUtilities.getPixelsFromDp(it, 24) }
            pixelsFromDp8?.let { pixelsFromDp24?.let { it1 -> layoutParams.setMargins(it, 0, it1, pixelsFromDp8) } }
            editTxt.layoutParams = layoutParams
            layoutEmail!!.addView(editTxt)
            nbrOfEmails++
        }
    }

    private fun createAddressEditTextDynamically(text: String) {
        if (nbrOfAddress < maxLength) {
            val editTxt = EditText(context)
            addressIdNmbr++
            editTxt.hint = resources.getString(R.string.address) + " " + addressIdNmbr
            editTxt.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
            editTxt.tag = "edit_text_address" + addressIdNmbr
            if (!TextUtils.isEmpty(text)) {
                editTxt.setText(text)
            }
            val layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT)
            val pixelsFromDp8 = context?.let { ViewUtilities.getPixelsFromDp(it, 8) }
            val pixelsFromDp24 = context?.let { ViewUtilities.getPixelsFromDp(it, 24) }
            pixelsFromDp8?.let { pixelsFromDp24?.let { it1 -> layoutParams.setMargins(it, 0, it1, pixelsFromDp8) } }
            editTxt.layoutParams = layoutParams
            layoutAddress!!.addView(editTxt)
            nbrOfAddress++
        }
    }

    private fun setPhoneInfoToUpdate(phoneNumbers: Array<String>) {
        for (i in phoneNumbers.indices) {
            if (i == 0) {
                phoneNumber!!.setText(phoneNumbers[i])
            } else {
                createPhoneEditTextDynamically(phoneNumbers[i])
            }
        }
    }

    private fun setEmailInfoToUpdate(emails: Array<String>) {
        for (i in emails.indices) {
            if (i == 0) {
                email!!.setText(emails[i])
            } else {
                createEmailEditTextDynamically(emails[i])
            }
        }
    }

    private fun setAddressInfoToUpdate(addresses: Array<String>) {
        for (i in addresses.indices) {
            if (i == 0) {
                email!!.setText(addresses[i])
            } else {
                createAddressEditTextDynamically(addresses[i])
            }
        }
    }

    override fun onDateSet(view: DatePicker, year: Int, month: Int, dayOfMonth: Int) {
        addContactViewModel.dobDateTime = LocalDateTime.of(year, month + 1, dayOfMonth, 0, 0)
        dateOfBirth!!.setText(addContactViewModel.dobDateTime.toString().substring(0, 10))
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater!!.inflate(R.menu.menu_toolbar, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.action_save -> if (null == contact) {//save
                addContactViewModel.contactPhone = ViewUtilities.getInputFromEditTexts(layoutPhone!!)
                addContactViewModel.contactEmail = ViewUtilities.getInputFromEditTexts(layoutEmail!!)
                addContactViewModel.contactAddress = ViewUtilities.getInputFromEditTexts(layoutAddress!!)
                if (allFieldsOk()) {
                    // TODO addContactViewModel.addContact();
                    activity?.finish()
                }
            } else {//update
                if (allFieldsOk()) {
                    activity?.finish()
                }
            }
            android.R.id.home -> activity?.finish()
            else -> {
            }
        }
        return true
    }
}
