package com.namdroid.vipchecker.ui.list

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.namdroid.vipchecker.model.Vip
import com.namdroid.vipchecker.repository.VipRepository
import io.reactivex.CompletableObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class ListViewModel @Inject constructor(private val vipRepository: VipRepository<Vip>): ViewModel() {

    fun getVips():LiveData<List<Vip>> {
        return vipRepository.getAll()
    }

    fun deleteContact(vip: Vip) {
        vipRepository.delete(vip)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onComplete() {
                        Timber.d("onComplete - deleted contact")
                    }

                    override fun onError(e: Throwable) {
                        Timber.e("OnError - deleted contact: "+ e.message)
                    }
                })
    }

}
