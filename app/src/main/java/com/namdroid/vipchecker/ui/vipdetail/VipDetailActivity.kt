package com.namdroid.vipchecker.ui.vipdetail

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View

import com.namdroid.vipchecker.R
import com.namdroid.vipchecker.databinding.VipDetailActivityBinding
import com.namdroid.vipchecker.model.Vip
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.vip_detail_activity.view.*
import javax.inject.Inject
import android.widget.Toast
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.vip_detail_activity.*
import kotlinx.android.synthetic.main.vip_item.*
import com.namdroid.vipchecker.R.id.imageView
import com.namdroid.vipchecker.ui.main.MainActivity




class VipDetailActivity : AppCompatActivity()  {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var picasso: Picasso

    private lateinit var viewModel: VipViewModel

    private lateinit var  binding:VipDetailActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.vip_detail_activity)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(VipViewModel::class.java)

        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        displayHomeAsUpEnabled()

        showVipDetail()
    }

    public override fun onDestroy() {
        super.onDestroy()
    }

    private fun displayHomeAsUpEnabled() {
        val actionBar = supportActionBar
        actionBar?.setDisplayShowHomeEnabled(true)
        actionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun showVipDetail() {
        val vipId = intent.getLongExtra(VIP_ID, 0)

        viewModel.getVip(vipId).observe(this, Observer { vip ->
            vip?.let { binding.vip = vip
                binding.toolbar.title = vip.displayName
                vip.address?.let { binding.root.address_group.visibility = View.VISIBLE }
                vip.email?.let {
                    if (!it.isBlank())
                        binding.root.email_group.visibility = View.VISIBLE
                }

                picasso.load(R.drawable.ic_person).into(vip_image, object : com.squareup.picasso.Callback {
                            override fun onSuccess() {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    startPostponedEnterTransition()
                                }
                            }

                            override fun onError() {
                                //do something when there is image loading error
                            }
                        })

                binding.notifyChange()
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_toolbar, menu)

        return true

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.getItemId()

        if (id == R.id.home) {
            onBackPressed()
            return true
        } else if (id == R.id.action_fav) {
            Toast.makeText(this@VipDetailActivity, "Action fav clicked", Toast.LENGTH_LONG).show()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    companion object {

        const val VIP_ID = "VIP_ID"

        fun startActivity(context: Context, vip: Vip) {
            val intent = Intent(context, VipDetailActivity::class.java)
            intent.putExtra(VIP_ID, vip.id)
            context.startActivity(intent)
        }

        fun makeIntent(context: Context, vip: Vip): Intent {
            val intent = Intent(context, VipDetailActivity::class.java)
            intent.putExtra(VIP_ID, vip.id)
            return intent
        }
    }
}
