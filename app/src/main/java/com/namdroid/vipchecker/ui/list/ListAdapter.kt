package com.namdroid.vipchecker.ui.list

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.namdroid.vipchecker.BR
import com.namdroid.vipchecker.R
import com.namdroid.vipchecker.model.Vip
import com.namdroid.vipchecker.utils.GlideApp
import kotlinx.android.synthetic.main.vip_item.view.*

class ListAdapter(clickListener: ClickListener) : RecyclerView.Adapter<ListAdapter.ListViewHolder>() {

    interface ClickListener {
        fun onClick(vip:Vip)
    }

    private var vips: List<Vip>
    private var itemClickListener: ClickListener? = null
    init {
        vips = emptyList()
        itemClickListener = clickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val viewDataBinding: ViewDataBinding? = DataBindingUtil.inflate(LayoutInflater.from(parent.context),
                R.layout.vip_item, parent, false)
        return ListViewHolder(viewDataBinding!!,this.itemClickListener)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val vip = vips[position]
        holder.bind(vip)
    }

    override fun getItemCount() = vips.size

    fun setItems(items: List<Vip>) {
        this.vips = items
        notifyDataSetChanged()
    }

    class ListViewHolder constructor(private val binding: ViewDataBinding,private val clickListener: ListAdapter.ClickListener?) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Vip) {
            binding.setVariable(BR.vip, item)
            binding.apply {
                binding.root.display_name.text = item.displayName
                binding.root.tag = item
                val uri = item.imageUri
                if (!(uri == null || uri.isEmpty())) {
                    GlideApp.with(binding.root.context).load(uri)
                            .placeholder(R.drawable.ic_account_circle_24dp)
                            .into(root.contact_image)
                }
                else {
                    val firstLetter:String = item.displayName?.subSequence(0, 1) as String
                    item.letterColor?.let {
                        root.contact_image.setPlaceholder(firstLetter,item.letterColor!!)
                    }

                }
            }
            binding.root.setOnClickListener { _ ->
                clickListener!!.onClick(item )
            }
        }

    }
}
