package com.namdroid.vipchecker.ui.list

import android.app.ActivityOptions
import android.app.SearchManager
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.SearchView
import android.view.*
import android.widget.Toast
import com.namdroid.vipchecker.R
import com.namdroid.vipchecker.databinding.VipListBinding
import com.namdroid.vipchecker.injection.Injectable
import com.namdroid.vipchecker.injection.ViewModelFactory
import com.namdroid.vipchecker.model.Vip
import com.namdroid.vipchecker.ui.list.ListAdapter.ClickListener
import com.namdroid.vipchecker.ui.vipdetail.VipDetailActivity
import kotlinx.android.synthetic.main.vip_item.*
import org.jetbrains.anko.AnkoLogger
import java.util.*
import javax.inject.Inject


class VipListFragment : Fragment(), Injectable, AnkoLogger {

    private lateinit var adapter: ListAdapter

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var vipListViewModel: ListViewModel
    private var vips: List<Vip>? = null
    lateinit var binding: VipListBinding

    private val itemClickListener = object : ClickListener {
        override fun onClick(vip: Vip) {

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                val p1 = android.util.Pair.create(contact_image as View, getString(R.string.thumbnail_transition_name))
                val p2 = android.util.Pair.create(display_name as View, context?.getString(R.string.display_name_transition_name))
                val p3 = android.util.Pair.create(phone as View, context?.getString(R.string.phone_transition_name))
                val options = ActivityOptions.makeSceneTransitionAnimation(activity, p1, p2, p3)
                val intent = activity?.let { VipDetailActivity.makeIntent(it, vip) }
                startActivity(intent, options.toBundle())
            } else {
                activity?.let { VipDetailActivity.startActivity(it, vip) }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        binding = VipListBinding.inflate(inflater, container, false)
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        adapter = ListAdapter(itemClickListener)
        vipListViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(ListViewModel::class.java)

        binding.recyclerView.adapter = this.adapter

        val dividerItemDecoration = DividerItemDecoration(binding.recyclerView.getContext(),
                DividerItemDecoration.HORIZONTAL)
        binding.recyclerView.addItemDecoration(dividerItemDecoration)

        vipListViewModel.getVips().observe(this, Observer { vips ->
            vips?.let {
                adapter.setItems(vips)
                this.vips = vips
            }
        })
    }


    private val deleteClickListener = View.OnClickListener { v ->
        val vip = v.tag as Vip
        vipListViewModel.deleteContact(vip)
    }


    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater!!.inflate(R.menu.menu_list, menu)

        // Associate searchable configuration with the SearchView
        val searchManager = activity?.getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = menu!!.findItem(R.id.action_search).actionView as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(activity?.componentName))


        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(s: String): Boolean {
                return false
            }

            override fun onQueryTextChange(s: String): Boolean {
                // Search Filter
                val filteredList = filter(vips, s)
                if (filteredList.isNotEmpty()) {
                    adapter.setItems(filteredList)
                    return true
                } else {
                    // If not matching search filter data
                    Toast.makeText(context, "Not Found", Toast.LENGTH_SHORT).show()
                    return false
                }
            }
        })
    }

    private fun filter(models: List<Vip>?, query: String): List<Vip> {
        var query = query
        query = query.toLowerCase().replace("\\s".toRegex(), "")
        val filteredModelList = ArrayList<Vip>()
        var names = ""
        for (model in models!!) {
            model.firstName?.let {
                names = model.firstName.toString().toLowerCase()
            }
            model.lastName?.let {
                names = names + " " + model.lastName.toString().toLowerCase()
            }

            names = names.replace("\\s".toRegex(), "")
            if (names.contains(query)) {
                filteredModelList.add(model)
            }
        }
        return filteredModelList
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted , Access contacts here or do whatever you need.
            }
        }
    }

    companion object {

        private val TAG = "VipListFragment"
        private val REQUEST_READ_CONTACTS = 0

        fun newInstance(): VipListFragment {
            return VipListFragment()
        }
    }
}
