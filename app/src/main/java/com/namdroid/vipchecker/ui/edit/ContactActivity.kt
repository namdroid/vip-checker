package com.namdroid.vipchecker.ui.edit

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

import com.namdroid.vipchecker.R
import com.namdroid.vipchecker.model.Contact


class ContactActivity : AppCompatActivity() {

    var contact: Contact? = null
        internal set

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_contact_activity)

        if (null != intent.extras) {
            contact = intent.extras!!.getParcelable(CONTACT)
        }
    }

    companion object {

        val CONTACT = "Contact"
    }
}
