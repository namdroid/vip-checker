package com.namdroid.vipchecker.ui.main

import android.arch.lifecycle.ViewModel
import com.namdroid.vipchecker.model.Vip
import com.namdroid.vipchecker.repository.VipRepository
import io.reactivex.CompletableObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class MainViewModel @Inject constructor(private val vipRepository: VipRepository<Vip>): ViewModel() {

    private var compositeDisposable:CompositeDisposable = CompositeDisposable()
    var  syncDisposable:Disposable? = null

    fun syncContacts() {

        if (syncDisposable == null || syncDisposable!!.isDisposed) {
            Timber.i("sync contacts ...")
            syncDisposable = vipRepository.sync()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnComplete {
                        Timber.d("onComplete - successfully sync vips")
                    }
                    .doOnError { e ->
                        Timber.e("onError - sync vips:" + e.message)
                    }
                    .subscribe()

            compositeDisposable.add(syncDisposable!!)
        }

    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}
