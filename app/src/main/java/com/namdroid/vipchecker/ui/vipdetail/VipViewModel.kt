package com.namdroid.vipchecker.ui.vipdetail

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.namdroid.vipchecker.injection.AppComponent
import com.namdroid.vipchecker.model.Vip
import com.namdroid.vipchecker.repository.VipRepository

import org.threeten.bp.LocalDateTime

import javax.inject.Inject


import io.reactivex.CompletableObserver
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class VipViewModel  @Inject constructor(private var vipRepository: VipRepository<Vip>): ViewModel() {

    var contactFirstName: String? = null
    var contactLastName: String? = null
    var dobDateTime: LocalDateTime? = null
    var contactPhone: String? = null
    var contactEmail: String? = null
    var contactAddress: String? = null

    fun updateContact(vip: Vip) {
        vipRepository.update(vip)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onComplete() {
                        Timber.d("onComplete - successfully updated contact")
                    }

                    override fun onError(e: Throwable) {
                        Timber.e("onError - update:" + e.message)
                    }
                })
    }

    fun getVip(id:Long): LiveData<Vip> {
        return vipRepository.get(id)
    }
}
