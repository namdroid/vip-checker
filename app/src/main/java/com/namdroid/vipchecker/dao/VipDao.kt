package com.namdroid.vipchecker.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update
import com.namdroid.vipchecker.model.Vip


@Dao
interface VipDao {

    @get:Query("SELECT * FROM " + Vip.TABLE_NAME)
    val all: LiveData<List<Vip>>

    @Query("SELECT * FROM " + Vip.TABLE_NAME + " WHERE phone_book_id LIKE :phoneBookId")
    fun getByPhoneBookId(phoneBookId:Int): Vip

    @Query("SELECT * FROM " + Vip.TABLE_NAME + " WHERE id LIKE :id")
    fun get(id:Long): LiveData<Vip>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertVips(vips: List<Vip>)

    @Delete
    fun delete(vip: Vip)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(entity: Vip)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(entity: Vip):Int

    @Query("SELECT COUNT(*) from " + Vip.TABLE_NAME)
    fun getCount(): Int
}
