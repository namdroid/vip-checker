package com.namdroid.vipchecker.injection

import com.namdroid.vipchecker.ui.list.ContactListFragment
import com.namdroid.vipchecker.ui.list.FavListFragment
import com.namdroid.vipchecker.ui.list.VipListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilderModule {

    @ContributesAndroidInjector
    abstract fun contributeVipListFragment(): VipListFragment

    @ContributesAndroidInjector
    abstract fun contributeFavListFragment(): FavListFragment


    @ContributesAndroidInjector
    abstract fun contributeContactListFragment(): ContactListFragment
}