package com.namdroid.vipchecker.injection

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable {
    // Empty on purpose
}