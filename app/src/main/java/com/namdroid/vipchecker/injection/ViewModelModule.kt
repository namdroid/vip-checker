package com.namdroid.vipchecker.injection

/**
 * Created by namdr on 12.02.2018.
 */

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.namdroid.vipchecker.ui.list.ListViewModel
import com.namdroid.vipchecker.ui.main.MainViewModel
import com.namdroid.vipchecker.ui.vipdetail.VipViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(vipListViewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VipViewModel::class)
    abstract fun bindVipViewModel(vipViewModel: VipViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ListViewModel::class)
    abstract fun bindVipListViewModel(vipListViewModel: ListViewModel): ViewModel
}