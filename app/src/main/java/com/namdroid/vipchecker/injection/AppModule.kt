package com.namdroid.vipchecker.injection

import android.arch.persistence.room.Room
import android.content.Context

import com.namdroid.vipchecker.VipCheckerApp
import com.namdroid.vipchecker.dao.VipDao
import com.namdroid.vipchecker.database.AppDatabase
import com.namdroid.vipchecker.repository.VipRepository
import com.namdroid.vipchecker.repository.VipRepositoryImpl

import javax.inject.Singleton

import dagger.Module
import dagger.Provides

import com.namdroid.vipchecker.database.DbMigration.MIGRATION_1_2
import com.namdroid.vipchecker.model.Vip
import com.squareup.picasso.Picasso

@Module(includes = [ViewModelModule::class])
class AppModule {

    @Provides
    @Singleton
    fun provideContext(application: VipCheckerApp): Context = application

    @Provides
    @Singleton
    fun providesVipRepository(vipDao: VipDao): VipRepository<Vip> {
        return VipRepositoryImpl(vipDao)
    }

    @Provides
    @Singleton
    fun providesContactsDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(context.applicationContext, AppDatabase::class.java,
                "contacts_db").addMigrations(MIGRATION_1_2).build()
    }

    @Provides
    @Singleton
    fun provideVipDao(appDatabase: AppDatabase): VipDao = appDatabase.vipDao()

    @Provides
    @Singleton
    fun providePicasso(context: Context): Picasso = Picasso.Builder(context).build()
}

