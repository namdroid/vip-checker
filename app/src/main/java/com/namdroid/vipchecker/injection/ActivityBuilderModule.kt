package com.namdroid.vipchecker.injection

import com.namdroid.vipchecker.ui.main.MainActivity
import com.namdroid.vipchecker.ui.vipdetail.VipDetailActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {

    @ContributesAndroidInjector(modules = [FragmentBuilderModule::class])
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun contributeVipDetailActivity(): VipDetailActivity
}