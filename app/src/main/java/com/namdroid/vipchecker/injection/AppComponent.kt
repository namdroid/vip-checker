package com.namdroid.vipchecker.injection

import com.namdroid.vipchecker.VipCheckerApp
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class,
    AndroidSupportInjectionModule::class,
    ActivityBuilderModule::class])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance fun application(vipCheckerApp: VipCheckerApp): Builder
        fun build(): AppComponent
    }

    fun inject(app: VipCheckerApp)
}
