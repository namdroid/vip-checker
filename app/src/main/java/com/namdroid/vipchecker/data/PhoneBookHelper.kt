package com.namdroid.vipchecker.data


import android.provider.ContactsContract
import com.namdroid.vipchecker.VipCheckerApp
import com.namdroid.vipchecker.model.Contact
import java.util.*


object PhoneBookHelper {

    //public static Observable<ContactResponse> getContacts() {
    //StringBuffer output;
    // Iterate every contact in the phone
    //counter = 0;
    //// Update the progress message
    //updateBarHandler.post(new Runnable() {
    //    public void run() {
    //        pDialog.setMessage("Reading contacts : "+ counter++ +"/"+cursor.getCount());
    //    }
    //});
    //This is to read multiple phone numbers associated with the same contact
    // Read every email id associated with the contact
    // TODO to change later
    val contacts: List<Contact>
        get() {
            val contactList = ArrayList<Contact>()

            val PHOTO_THUMBNAIL_URI = ContactsContract.Profile.PHOTO_THUMBNAIL_URI
            val CONTENT_URI = ContactsContract.Contacts.CONTENT_URI
            val _ID = ContactsContract.Contacts._ID
            val DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME
            val DISPLAY_NAME_PRIMARY = ContactsContract.Contacts.DISPLAY_NAME_PRIMARY
            val PHONETIC_NAME = ContactsContract.Contacts.PHONETIC_NAME
            val DISPLAY_NAME_SOURCE = ContactsContract.Contacts.DISPLAY_NAME_SOURCE
            val HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER
            val PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI
            val Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID
            val NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER
            val EmailCONTENT_URI = ContactsContract.CommonDataKinds.Email.CONTENT_URI
            val EmailCONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID
            val DATA = ContactsContract.CommonDataKinds.Email.DATA
            val contentResolver = VipCheckerApp.vipApp!!.contentResolver

            val sortOrder = "LOWER (" + ContactsContract.Profile.DISPLAY_NAME + ") ASC"
            val SELECTION = ContactsContract.Contacts.IN_VISIBLE_GROUP + " = '" + "1" + "'" + " AND " + ContactsContract.Contacts.HAS_PHONE_NUMBER

            //val SELECTION = DISPLAY_NAME_PRIMARY + " AND " + ContactsContract.Contacts.HAS_PHONE_NUMBER

            val cursor = contentResolver.query(CONTENT_URI, null, SELECTION, null, sortOrder)

            if (cursor.count > 0) {
                while (cursor.moveToNext()) {
                    var phoneNumber: String = ""
                    var email: String = ""
                    val contact_id = cursor.getString(cursor.getColumnIndex(_ID))
                    val displayname = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME_PRIMARY))

                    val hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER)))
                    if (hasPhoneNumber > 0) {

                        val people = Contact()
                        // fetch first phone number
                        var firstPhone: String? = ""
                        val phoneCursor = contentResolver.query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", arrayOf(contact_id),
                                ContactsContract.CommonDataKinds.Phone.SORT_KEY_PRIMARY)
                        while (phoneCursor.moveToNext()) {
                            phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER))
                            if (firstPhone!!.isBlank()) {
                                if (phoneNumber.contains("*") || phoneNumber.contains("#")) {
                                    continue
                                }
                                firstPhone = phoneNumber
                                firstPhone = firstPhone.replace("\\s".toRegex(), "")
                                break
                            }
                        }
                        phoneCursor.close()

                        // fetch first email
                        val emailCursor = contentResolver.query(EmailCONTENT_URI, null, EmailCONTACT_ID + " = ?", arrayOf(contact_id), null)
                        while (emailCursor!!.moveToNext()) {
                            val columnIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS)
                            if (columnIndex != -1) {
                                email = emailCursor.getString(columnIndex)
                            }
                        }
                        emailCursor.close()


                        val photouri = cursor.getString(cursor.getColumnIndex(ContactsContract.Profile.PHOTO_THUMBNAIL_URI))

                        people.phoneBookId = Integer.valueOf(contact_id)
                        if (displayname != null) {
                            people.displayName = displayname
                            people.firstName = displayname
                        }
                        if (email != null) {
                            people.email = email
                        }
                        people.phoneNumber = firstPhone
                        people.imageUri = photouri

                        val postal_uri = ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_URI
                        val postal_cursor = contentResolver.query(postal_uri, null, ContactsContract.Data.CONTACT_ID + "=" + contact_id, null, null)

                        while (postal_cursor!!.moveToNext()) {
                            val street = postal_cursor.getString(postal_cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.STREET))
                            val city = postal_cursor.getString(postal_cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.CITY))
                            val state = postal_cursor.getString(postal_cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY))
                            people.address = "$street, $city, $state"
                            break
                        }
                        postal_cursor.close()

                        contactList.add(people)
                    }

                }
            }

            cursor.close()

            return contactList
        }

    /*
    fun searchContactsByName(query: String): List<Contact> {
        val result = TreeSet<Contact>()

        val cursor = listContactsByName(query)

        if (cursor != null && cursor.moveToFirst()) {
            val previous = ""
            val actual: Contact? = null

            do {
                result.add(mapContact(cursor))
            } while (cursor.moveToNext())
        }

        cursor!!.close()

        return ArrayList(result)
    }

    internal fun listContactsByName(query: String): Cursor? {
        val contentResolver = VipCheckerApp.vipApp!!.contentResolver

        // Sets the columns to retrieve for the user profile
        val projection = arrayOf(ContactsContract.Profile._ID, ContactsContract.Profile.DISPLAY_NAME_PRIMARY, ContactsContract.Profile.LOOKUP_KEY, ContactsContract.Profile.PHOTO_THUMBNAIL_URI)

        val selection = ContactsContract.Profile.DISPLAY_NAME_PRIMARY + " LIKE ? AND " + (ContactsContract.Contacts.IN_VISIBLE_GROUP + " = '" + "1" + "'")

        val selectionArgs = arrayOf("%" + (query + "%"))
        val sortOrder = "LOWER (" + ContactsContract.Profile.DISPLAY_NAME_PRIMARY + ") ASC"

        // Retrieves the profile from the Contacts Provider
        return contentResolver.query(
                ContactsContract.Contacts.CONTENT_URI,
                projection,
                selection + " AND " + ContactsContract.Contacts.HAS_PHONE_NUMBER,
                selectionArgs,
                sortOrder)
    }

    fun listAllContacts(): Cursor? {
        val contentResolver = VipCheckerApp.vipApp!!.contentResolver

        // Sets the columns to retrieve for the user profile
        val projection = arrayOf(ContactsContract.Profile._ID, ContactsContract.Profile.DISPLAY_NAME_PRIMARY, ContactsContract.Profile.LOOKUP_KEY, ContactsContract.Profile.PHOTO_THUMBNAIL_URI, ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Email.DATA)

        val selection = ContactsContract.Contacts.IN_VISIBLE_GROUP + " = '" + "1" + "'"
        val selectionArgs: Array<String>? = null
        val sortOrder = "LOWER (" + ContactsContract.Profile.DISPLAY_NAME_PRIMARY + ") ASC"

        // Retrieves the profile from the Contacts Provider
        return contentResolver.query(
                ContactsContract.Contacts.CONTENT_URI,
                projection,
                selection + " AND " + ContactsContract.Contacts.HAS_PHONE_NUMBER,
                selectionArgs,
                sortOrder)
    }

    private fun mapContact(c: Cursor): Contact {
        val _ID = c.getColumnIndex(ContactsContract.Profile._ID)
        val DISPLAY_NAME_PRIMARY = c.getColumnIndex(ContactsContract.Profile.DISPLAY_NAME_PRIMARY)
        val PHOTO_THUMBNAIL_URI = c.getColumnIndex(ContactsContract.Profile.PHOTO_THUMBNAIL_URI)
        val phoneNumber = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
        val email = c.getString(
                c.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA))
        // String emailType = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));
        val people = Contact()
        val name = c.getString(DISPLAY_NAME_PRIMARY)
        val photoUri = c.getString(PHOTO_THUMBNAIL_URI)
        people.phoneBookId = Integer.valueOf(c.getString(_ID))
        people.displayName = name
        if (email != null) people.email = email
        people.phoneNumber = phoneNumber
        people.imageUri = photoUri

        return people
    }


    fun getIndexList(list: List<Contact>): CharArray {
        val result = CharArray(list.size)
        var i = 0

        for (p in list) {
            result[i] = Character.toUpperCase(p.firstName!![0])
            i++
        }

        return result
    }
    */
}