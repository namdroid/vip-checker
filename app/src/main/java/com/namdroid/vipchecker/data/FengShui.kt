package com.namdroid.vipchecker.data

import com.namdroid.vipchecker.model.Hexagram
import com.namdroid.vipchecker.model.HexagramLevel
import timber.log.Timber

/**
 * Created by namnguyen on 26.07.17.
 */
object FengShui {
    var PHONGTHUY: Array<Array<String>>

    init {
        val arrayOfString1 = arrayOf(HexagramLevel.VERY_GOOD.name, "Thiên địa thái bình ", "Số người phú quí, trường thọ đại kiết tường chi vận cách. Vinh hoa, danh dự chung thân hưởng phước, thường nhân khó được số này vậy.")
        val arrayOfString2 = arrayOf(HexagramLevel.BAD.name, "Không phân định ", "Thưở trời đất chưa minh định. Tam tài chưa thuyết minh, chẳng lấy gì làm căn bản và lập qui củ, nên thường xảy ra tiến thoái không quyết đoán trước được.")
        val arrayOfString3 = arrayOf(HexagramLevel.VERY_GOOD.name, "Tấn tới như ý ", "Mọi việc tiến là thu hoạch như ý, danh lợi làm nên, nở mặt với đời như được thiên chức trong phú quý, thật là đại kiết số vậy.")
        val arrayOfString4 = arrayOf(HexagramLevel.BAD.name, "Bệnh ", "Mọi sự hoạt động đều bị khiếm khuyết, đại khổ nạn, tai họa liên miên, cực nhọc mà không làm nên việc gì hoàn chỉnh.")
        val arrayOfString5 = arrayOf(HexagramLevel.GOOD.name, "Trường Thọ ", "Trùng hưng tổ nghiệp, thật là phú quý vinh đạt, làm nên danh dự cho bản thân và toàn gia.")
        val arrayOfString6 = arrayOf(HexagramLevel.GOOD.name, "Sống an nhàn dư giả ", "Được phước trời cho, năng hưng gia lập nghiệp đại phú, đại danh dự,an ổn nhàn định.")
        val arrayOfString7 = arrayOf(HexagramLevel.BAD.name, "Cương nghị quyết đoán ", "Người mang số này cứng cỏi không dời đổi, nếu quyết là thẳng thắn làm việc quá cứng cỏi nên thường bị đổ dở vậy.")
        val arrayOfString8 = arrayOf(HexagramLevel.GOOD.name, "Ý Chí kiên cường", "Trước sau không dời đổi, ý chí vui vẻ, mọi việc làm tiến từ từ, từng bước chậm, mỗi bước là chắc chắn thành công. ")
        val arrayOfString9 = arrayOf(HexagramLevel.BAD.name, "Hưng tân cúc khai ", "Số này trước tốt sau xấu, nếu phạm phải thì gánh lấy tai ương, gặp toàn bất hạnh. ")
        val arrayOfString10 = arrayOf(HexagramLevel.BAD.name, "Vạn sự kết cục ", "Một chuổi đời ảm đạm, khó tìm một ánh sáng quang minh. Thật là vận số tận cùng. Tứ cố vô thân, gởi xương nơi đất khách. ")
        val arrayOfString11 = arrayOf(HexagramLevel.GOOD.name, "Gia vận tốt", "Có dịp tái hưng gia nghiệp, quyến thuộc sum vầy, có dưỡng tử tốt, phát đại phú vinh vậy. ")
        val arrayOfString12 = arrayOf(HexagramLevel.BAD.name, "Ý chí mềm yếu ", "Khó thành công toàn vẹn tròn trên đường lập nghiệp. Mọi ý nghĩ không thích hợp với sự việc nên gặp thất bại luôn. ")
        val arrayOfString13 = arrayOf(HexagramLevel.GOOD.name, "Tài chí hơn người ", "Có trí mưu lược, bền sức chịu đựng để lấy sự thành công, phải hưởng thụ sự phú quý vinh hoa. Hào vận đáng mong cầu vậy. ")
        val arrayOfString14 = arrayOf(HexagramLevel.BAD.name, "Nước mắt thiên nhạn ", "Không người giúp đở gặp cảnh cô độc, khó thấy niềm vui nào mang đến, cuộc đời như đá chìm đáy nước, chịu một đời tăm tối đau buồn. ")
        val arrayOfString15 = arrayOf(HexagramLevel.GOOD.name, "Đạt được phước thọ ", "Được người trên bước nâng đỡ, nên thân thế thành đại sự nghiệp, nắm lấy sự thành công phú quý vinh đạt, số này ít có người được vậy. ")
        val arrayOfString16 = arrayOf(HexagramLevel.GOOD.name, "Quế nhân phò trợ ", "Được thành vị thủ lãnh, chúng nhơn tôn vinh, thành tựu đại sự nghiệp, phú quý vinh đạt")
        val arrayOfString17 = arrayOf(HexagramLevel.NORMAL.name, "Vượt qua mọi khó khăn ", "Nên lấy ôn hòa làm căn bản, dẹp bớt sự cứng rắn quá độ, mới đủ khí phách nắm lấy đại thành công, đề phòng bị người mưu hại. ")
        val arrayOfString18 = arrayOf(HexagramLevel.NORMAL.name, "Bình thường ", "Với ý chí kiên trì, năng bài trừ mọi chướng ngại, khắc phục mọi khó khăn để đạt thành mục đích nhất định thành công. ")
        val arrayOfString19 = arrayOf(HexagramLevel.NORMAL.name, "Đoàn tụ ông bà", "Trên bước đường lập nghiệp phải vượt mọi khốn khó và được quý nhân giúp đở mới đạt thành chí nguyện, có thể là phú Ông. Song khó tránh chết yểu, cô quả, hình ngục. ")
        val arrayOfString20 = arrayOf(HexagramLevel.BAD.name, "Phí nghiệp vỡ vận ", "Một đời chỉ thấy toàn đổ dở, trái ý, gặp nhiêu khó khăn không thể nào tả xiết. ")
        val arrayOfString21 = arrayOf(HexagramLevel.NORMAL.name, "Minh nguyệt quay chiều", "Nhờ nhân lực mà được vươn mình dưới ánh nắng mùa xuân, hoa mai vấn đượm sắc hương trời xuân huy tráng lệ. Được chúng nhơn suy tôn, đưa đến địa vị độc lập (Nam tốt, Nữ kỵ)")
        val arrayOfString22 = arrayOf(HexagramLevel.NORMAL.name, "Mặt trời mọc ", "Công danh hiển đạt, tột đỉnh vinh sang. Nhờ sức chụi đựng vượt qua mọi trở ngại và khéo xử sự khôn ngoan (Nam tốt, Nữ kỵ)")
        val arrayOfString23 = arrayOf(HexagramLevel.GOOD.name, "Gia môn khánh dư ", "Có tài năng trong lãnh vực tham mưu, phát minh mọi sáng kiến, làm nên sự việc hiển vinh cho con cháu, thật là một điều hỷ khánh kiết tường vậy. ")
        val arrayOfString24 = arrayOf(HexagramLevel.GOOD.name, "Tính cách anh mẫn ", "Tánh tình khách quan mẫn tiệp, có tài năng lanh lợi, nên dễ lấy sự thành công lớn lao do vận số. ")
        val arrayOfString25 = arrayOf(HexagramLevel.NORMAL.name, "Biến hóa kỳ dị ", "Ly nạn lụy thân. Ấy là số chụi suốt đời vượt trên sự chết mà tìm đất sống nhưng cũng không yên thân. ")
        val arrayOfString26 = arrayOf(HexagramLevel.NORMAL.name, "Dục vọng vô tận ", "Do tâm tánh quá cứng cỏi, nên gặp cảnh giúp đời mà không ai biết ơn, và lắm sự gây đổ. Số này phát đạt ở hậu vận. ")
        val arrayOfString27 = arrayOf(HexagramLevel.NORMAL.name, "Tự hào sinh lý ", "Số này con cháu hoang tàn bạo ngược. Tai họa dập dồn, chịu nhiều thống khổ thân tâm. ")
        val arrayOfString28 = arrayOf(HexagramLevel.NORMAL.name, "Dục vọng khó khăn", "Lúc nào cũng thiếu thốn, như muốn thâu gom về một mối cây. Tâm hòa hiệp , ham muốn rộng lớn như rồng trên mây, nắm lấy nhiều cơ hội thành công. ")
        val arrayOfString29 = arrayOf(HexagramLevel.BAD.name, "Chết đi sống lại", "Lúc thắng lúc bại khó phân. Một phen mạo hiểm tronng tuyệt tử, phùng sanh mà thành công. Cuộc đời cô độc khắc vợ con. ")
        val arrayOfString30 = arrayOf(HexagramLevel.GOOD.name, "Tài dũng được chí", "Có sự kiên cố trong mọi ý chí, năng xông pha mọi thử thách, làm nên danh lợi vĩ đại cho sự nghiệp. Truyền đắc danh lợi phú quý. Có tài năng chỉ huy, nhiều đức độ sung vinh. ")
        val arrayOfString31 = arrayOf(HexagramLevel.NORMAL.name, "Ước thấy", "Rồng vàng còn phải nằm mong đợi, vì thời vận chưa đến lúc. Gắp lúc phong vân tuyết nguyệt thì bay lên không trung mà tuung hoành. Có ý thành thật, biết dung hòa nên được người trên bước nâng đở, thuận lợi phát đạt, thành công lớn. ")
        val arrayOfString32 = arrayOf(HexagramLevel.NORMAL.name, "Gia môn hưng thịnh ", "Quyết chí không lùi, trên bước đường khai cơ lập nghiệp, thành tựu đại sựu nghiệp, được hưởng danh lợi vẹn toàn, huy danh chói sáng trong thiên hạ, bậc thường nhơn khó được. ")
        val arrayOfString33 = arrayOf(HexagramLevel.BAD.name, "Gia đình tan vỡ ", "Suốt đời tai nạn chẳng dứt, gia đình ly biệt, sát hại, phát cuồng, số này phải chịu cảnh đối khổ, bần tiện. ")
        val arrayOfString34 = arrayOf(HexagramLevel.NORMAL.name, "Bình an ôn hòa ", "Với mọi ý chí và nổ lực để giữ lấy sự thành công. Chớ nên ỷ lại tha nhơn giúp đỡ. ")
        val arrayOfString35 = arrayOf(HexagramLevel.BAD.name, "Phong ba không ngừng", "Trước mặt là con đường mờ mịt, trở lại thì không ổn vì phong ba dấy khởi ngập trời. ")
        val arrayOfString36 = arrayOf(HexagramLevel.GOOD.name, "Hiển đạt uy quyền ", "Được chúng nhơn trông cậy, trước sau không dời đổi. Hay khắc phục khó khăn để thành công sự nghiệp đại phú đại quý vẹn toàn. ")
        val arrayOfString37 = arrayOf(HexagramLevel.BAD.name, "Ý chí mềm yếu", "Đừng mong gặp thành công lớn, vì không đủ năng lực đi đến đích. Đặc biệt số này thành tựu về văn chương sách báo. ")
        val arrayOfString38 = arrayOf(HexagramLevel.GOOD.name, "Vinh hoa phú quý ", "Một đời phú quý vinh hoa, phúc lộc dài dài, con cháu đời đời nối dõi, hưởng phú quý long xương. Thật là một số rất quý trọng. ")
        val arrayOfString39 = arrayOf(HexagramLevel.NORMAL.name, "Cẩn thận được an", "Số này có mưu trí hơn người, nhưng không hưởng được, vì không hợp với từng sự việc, ấy là nguyên nhân thất bại. Nếu biết ôn tồn dè dặt thì cũng phát đạt và bình an. ")
        val arrayOfString40 = arrayOf(HexagramLevel.GOOD.name, "Đức vọng cao thượng ", "Tất cả danh dự trên đời đều được mà không phải cầu người trên, chỉ biết nổ lực làm việc, bước chậm và vững chắc ý, tiến mãi không có giới hạn. ")
        val arrayOfString41 = arrayOf(HexagramLevel.BAD.name, "Ngày không thành", "Tân tụy đem tâm nghiên cứu, nhưng trong mười nghề có hết chín chẳng thành. Số này khi lớn tuổi phải chịu sự cô độc. ")
        val arrayOfString42 = arrayOf(HexagramLevel.BAD.name, "Hoa trong như đêm", "Như hoa vào lúc nửa đêm tơi tả lạnh lung, tuy có tài nâng nhung gặp dịp may có một thời. Số phải chết xa nhà và đi trong cảnh nghịch. ")
        val arrayOfString43 = arrayOf(HexagramLevel.BAD.name, "Buồn tủi cực  ", "Ảm đạm thảm khổ vô ngần, mọi việc đều thất bại, nên cẩn thận thì bớt nỗi thê lương. ")
        val arrayOfString44 = arrayOf(HexagramLevel.GOOD.name, "Vận tốt ", "Thuận gió xuôi buồm, làm rạng danh trong thiên hạ, bậc đại ý chí, thành đại sự nghiệp vậy. ")
        val arrayOfString45 = arrayOf(HexagramLevel.BAD.name, "Gặp nhiều chuyển biến", "Một đời khốn khổ, sầu não đeo mang, khó tìm ra hạnh phúc, mãi chìm trong thất bại. ")
        val arrayOfString46 = arrayOf(HexagramLevel.GOOD.name, "Khai hoa nở nhụy", "Y thực tự nhiên đủ, có hưởng phần thiên chức lẫn hạnh phúc, tự tại một nhà viên mãn. ")
        val arrayOfString47 = arrayOf(HexagramLevel.GOOD.name, "Lập chí ", "Đáng là vị cố vấn, đáng thọ lãnh cao chức, giàu sang oai vọng vinh đạt, ở hàng cao phẩm trong loài người. ")
        val arrayOfString48 = arrayOf(HexagramLevel.BAD.name, "Nhiều điều xấu ", "Đương số phải chịu nhiều tổn thất và tai họa xảy đến củng không ít trong cuộc đời. ")
        val arrayOfString49 = arrayOf(HexagramLevel.BAD.name, "Một thành một bại ", "Một đời thành đạt tột đỉnh, rồi sau đó chuyển sang thất bại lạc loài, như cánh hoa chỉ nở một thời rồi tàn cả, rã rời theo gió. ")
        val arrayOfString50 = arrayOf(HexagramLevel.BAD.name, "Thịnh yên suông sẽ", "Thạnh suy qua lại chẳng lường được. ")
        val arrayOfString51 = arrayOf(HexagramLevel.GOOD.name, "Biết trước được việc ", "Có một sự quang minh hiện lên trước mắt, khéo léo tài năng. Biết trước thời thế, nhờ đôi mắt tinh anh lịch lãm, nên thành công phải đến, danh lợi song toàn như ý. ")
        val arrayOfString52 = arrayOf(HexagramLevel.BAD.name, "Không thiếu mạng, không do vận may. ", "Đại hung chi số. Nhiều chướng nạn chẳng dứt, suốt đời chỉ chuốt lấy thất bại nghẹn ngào. ")
        val arrayOfString53 = arrayOf(HexagramLevel.BAD.name, "Ngoài tốt trong khổ ", "Bên ngoài xem như sự vui vẻ lớn lao, nhưng bên trong chứa những chua cay ai biết. ")
        val arrayOfString54 = arrayOf(HexagramLevel.BAD.name, "Thảm thương", "Tính toán một đàng, sự việc về một nẽo khác, vậy nên khó hoàn thành mọi việc như ý. ")
        val arrayOfString55 = arrayOf(HexagramLevel.NORMAL.name, "Cây thông trong vườn tuyết ", "Cây thành tùng giỏi chịu tuyết sương suốt muà Đông giá buốt. Có khí phách sinh kế, gặp lắm vất vả và phải chịu một phen vượt qua một tai nạn lớn, sao đó phát đạt lớn, hưởng vinh hoa hạnh phúc lẫn thiên chức vậy. ")
        val arrayOfString56 = arrayOf(HexagramLevel.NORMAL.name, "Khổ trước khỏe sau ", "Phải trải qua 1 lần hoạn nạn lớn, sau mới tái hưng gia nghiệp, giàu có vinh sang tột bực, do kinh nghiệm thử thách cuộc đời. ")
        val arrayOfString57 = arrayOf(HexagramLevel.BAD.name, "Mất phương hướng ", "Xe yếu mà lên dốc cao, người không có bền sức trong mọi hành vi, một đời chẳng nên việc gì. ")
        val arrayOfString58 = arrayOf(HexagramLevel.BAD.name, "Tối tăm không ánh sang ", "Một màu đen thăm thẳm, mờ mịt không phương hướng. Gặp toàn sự thất bại và bất hạnh. ")
        val arrayOfString59 = arrayOf(HexagramLevel.GOOD.name, "Danh lợi đầy đủ ", "Danh lợi nhất cữ lưỡng đắc, phồn vinh phú quý chi kiết vận nhờ xử sự tinh tế mà nắm lấy hạnh phúc, thọ thiên chức, mẫu người đáng chỗ cầu mong. ")
        val arrayOfString60 = arrayOf(HexagramLevel.BAD.name, "Căn bản yếu kém", "Có sở hoạt động lần hồi sa sút, mỗi bước mỗi thất bại, việc trái ý đến luôn. ")
        val arrayOfString61 = arrayOf(HexagramLevel.GOOD.name, "Đạt được vinh hoa phú quý ", "Mọi sự đều được như ý. Hiển vinh phú quý, truyền lưu tử tôn, đời đời hưởng hạnh phúc. ")
        val arrayOfString62 = arrayOf(HexagramLevel.BAD.name, "Cốt nhục chia cắt", "Gia thế chẳn lúc nào yên ổn, cốt nhục phân ly, qua rồi một thời hưng thịnh, việc sinh nhai thật là chẳng ổn chút nào. ")
        val arrayOfString63 = arrayOf(HexagramLevel.GOOD.name, "Phú quý trường thọ ", "Giàu sang trường thọ đủ đầy, danh lợi cao xa, phước lộc đầy nhà, thành công như ý. ")
        val arrayOfString64 = arrayOf(HexagramLevel.BAD.name, "Trong ngoài không hòa nhã", "Mỗi bước tiến cũng như thoái, đều là việc chẳng lành, trong ngoài bất hòa, mất cả niềm tin, đi đến cảnh phá sản không thể tránh được. ")
        val arrayOfString65 = arrayOf(HexagramLevel.GOOD.name, "Đường lợi thông suốt", "Trên đường kiến gia lập nghiệp, phồn vinh phú quý nay đến, hưởng thọ lợi lộc hanh thong. ")
        val arrayOfString66 = arrayOf(HexagramLevel.GOOD.name, "Lập nghiệp thương gia ", "Trí tuệ thong minh, sự nghiệp phát đạt, được mọi người tín nhiệm, phú quý danh dự chơn thật trong đạo đức tốt lành. ")
        val arrayOfString67 = arrayOf(HexagramLevel.BAD.name, "Đứng ngồi không yên ", "Ngồi đứng không an, gặp toàn hoạn nạn. ")
        val arrayOfString68 = arrayOf(HexagramLevel.BAD.name, "Diệt vong thế hệ ", "Đã bần khổ lại thêm hoạn nạn triền miên. ")
        val arrayOfString69 = arrayOf(HexagramLevel.GOOD.name, "Hưởng tinh thần chịu khó ", "Được hạnh phúc an thái, không có gì phiền. ")
        val arrayOfString70 = arrayOf(HexagramLevel.NORMAL.name, "Suối vàng chờ đợi", "Lợi tốt thì họa liền theo, vui buồn lẫn lộn khó phân, trước tốt sau xấu. ")
        val arrayOfString71 = arrayOf(HexagramLevel.NORMAL.name, "Ý chí cao mà sức yếu ", "Ý chí thì cao xa rộng lớn, mà năng lực lại nhỏ hẹp thấp kém, tuy nhiên hưởng được thiên chức, sống trong cảnh an nhàn. ")
        val arrayOfString72 = arrayOf(HexagramLevel.BAD.name, "Hoàn cảnh gặp bất trắc ", "Không có khả năng về sinh nhai, một đời nổi chìm trong nghịch cảnh, thương tâm. ")
        val arrayOfString73 = arrayOf(HexagramLevel.NORMAL.name, "Thủ được binh an ", "Thấp thỏm về sự nghiệp tầm thường. Nếu bị động thì nên duyên theo đó mà tùy người được phước, nên thủ chớ nên nóng. ")
        val arrayOfString74 = arrayOf(HexagramLevel.BAD.name, "Vĩnh biệt ngàn thu", "Số vợ con tử biệt, cốt nhục chia lìa. ")
        val arrayOfString75 = arrayOf(HexagramLevel.NORMAL.name, "Vui sướng cực đỉnh", "Vui rồi lại buồn, trước hưởng hạnh phúc, sau đó lại rơi vào cảnh bất hạnh. ")
        val arrayOfString76 = arrayOf(HexagramLevel.BAD.name, "Gia đình buồn tủi", "Tuổi nhỏ phát đạt giàu sang, tuổi già gặp cảnh đói khổ, đời sống đầy bi thương. ")
        val arrayOfString77 = arrayOf(HexagramLevel.BAD.name, "Hồi sức ", "Một đời thất bại không làm sao cứu được. ")
        val arrayOfString78 = arrayOf(HexagramLevel.BAD.name, "Gặp nhiều xui xẻo", "Một đời chỉ chuốt lấy khó khăn cực nhọc. ")
        val arrayOfString79 = arrayOf(HexagramLevel.GOOD.name, "Hồi quy trung phước", "Vạn vật hoàn về điểm khởi thì kiết tường chí cực danh dương hải, phú quý vô ti, kiết tường mang đến sự tôn quý tự nhiên. ")
        PHONGTHUY = arrayOf(arrayOfString1, arrayOfString2, arrayOfString3, arrayOfString4, arrayOfString5, arrayOfString6, arrayOfString7, arrayOfString8, arrayOfString9, arrayOfString10, arrayOfString11, arrayOfString12, arrayOfString13, arrayOfString14, arrayOfString15, arrayOfString16, arrayOfString17, arrayOfString18, arrayOfString19, arrayOfString20, arrayOfString21, arrayOf(HexagramLevel.NORMAL.name, "Thiên thai phụng sương ", "Mọi việc đều gặp sự trái ý muốn, như cây cỏ mùa thu bị ẩm ướt lại gặp sương rơi. Một chuỗi ngày thê lương, bệnh hoạn cô độc, lo lắng nhiều nỗi, cuộc đời bất hạnh. "), arrayOfString22, arrayOfString23, arrayOfString24, arrayOfString25, arrayOfString26, arrayOfString27, arrayOfString28, arrayOfString29, arrayOfString30, arrayOfString31, arrayOfString32, arrayOfString33, arrayOfString34, arrayOfString35, arrayOfString36, arrayOfString37, arrayOfString38, arrayOfString39, arrayOfString40, arrayOfString41, arrayOfString42, arrayOfString43, arrayOfString44, arrayOfString45, arrayOfString46, arrayOfString47, arrayOfString48, arrayOfString49, arrayOfString50, arrayOfString51, arrayOf(HexagramLevel.BAD.name, "Nội tâm ưu sầu ", "Thấy bên ngoài như phước lộc đầy nhà, mà bên trong lại đầy sự khốn khó, ưu phiền. "), arrayOfString52, arrayOfString53, arrayOfString54, arrayOfString55, arrayOfString56, arrayOfString57, arrayOfString58, arrayOfString59, arrayOfString60, arrayOfString61, arrayOfString62, arrayOfString63, arrayOfString64, arrayOfString65, arrayOfString66, arrayOfString67, arrayOfString68, arrayOfString69, arrayOfString70, arrayOfString71, arrayOfString72, arrayOfString73, arrayOfString74, arrayOfString75, arrayOfString76, arrayOfString77, arrayOfString78, arrayOfString79)
    }


    fun getHexagram(phone: String): Hexagram? {
        var hexagram: Hexagram? = null
        val phoneNumber = phone.replace("+","00")
        if (isNumeric(phoneNumber)) {
            // Lấy 4 số cuối là 9666:80 = 120,825, lấy 120,825 – 120 = 0,825, sau đó lấy 0.8255 x 80 =66 (tra mục theo bảng với số 66) với ý nghĩa là: "Mọi việc như ý, phú quý tự đến" - Đại cát.

            if (phoneNumber.length in 4..20) {
                val lastFour = Integer.valueOf(phoneNumber.substring(phoneNumber.length - 4))!!
                val divideResult = lastFour.toFloat() / 80
                val hole = lastFour / 80
                val rest = divideResult - hole

                val magicNumber = Math.round(rest * 80)

                hexagram = when {
                    magicNumber == 0 -> {
                        Timber.e("magicNumber = 0, phone = " + phoneNumber)
                        Hexagram(0, HexagramLevel.NORMAL, "", "")
                    }
                    magicNumber >= 80 -> {
                        Timber.e("magicNumber >= 80, phone = " + phoneNumber)
                        Hexagram(79, HexagramLevel.valueOf(FengShui.PHONGTHUY[79][0]), FengShui.PHONGTHUY[79][1], FengShui.PHONGTHUY[79][2])
                    }
                    else -> Hexagram(magicNumber, HexagramLevel.valueOf(FengShui.PHONGTHUY[magicNumber][0]), FengShui.PHONGTHUY[magicNumber][1], FengShui.PHONGTHUY[magicNumber][2])
                }
            }
        }

        return hexagram
    }

    private fun isNumeric(paramString: String): Boolean {
        return paramString.matches("[-+]?\\d*\\.?\\d+".toRegex())
    }
}
